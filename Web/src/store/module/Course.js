import http from "../../http-common";
import profileManager from "./ProfileManager";
const courseManament = {
  state: {
    courses: [],
    course: {},
    pagingCourse: {},
    courseDetail: [],
  },
  getters: {
    getCourses: (state) => {
      return state.courses;
    },
    getPagingCourse: (state) => {
      return state.pagingCourse;
    },
    getCourseDetail: (state) => {
      return state.courseDetail;
    },
    getCourse: (state) => {
      return state.course;
    },
  },
  mutations: {
    setCourses(state, courses) {
      if (courses != undefined) {
        state.courses = courses;
      } else {
        state.courses = [];
      }
    },
    setPagingCourse(state, paging) {
      state.pagingCourse = paging;
    },
    updateCourse(state, course) {
      const index = state.courses.findIndex((c) => c.id == course.id);
      if (index != -1) {
        state.courses.splice(index, 1, course);
      }
    },
    setCourseDetail(state, courseDetail) {
      state.courseDetail = courseDetail;
    },
    setCourse(state, course) {
      return (state.course = course);
    },
  },
  actions: {
    async getAllCoursesByAdmin(context, filter) {
      try {
        const response = await http.get(`/courses/filter`, {
          params: {
            ManagerId: filter.managerId,
            SubjectName: filter.subjectName,
            CourseName: filter.courseName,
            TutorName: filter.tutorName,
            Status: filter.status,
            FromDate: filter.fromDate,
            ToDate: filter.toDate,
            PageNumber: filter.pageNumber,
          },
        });

        if (response.status != 404) {
          let courses = response.data.data;
          let paging = response.data;
          context.commit("setCourses", courses);
          context.commit("setPagingCourse", paging);
        }
        return response;
      } catch (error) {
        console.log(error);
      }
    },
    async disableCourseStatus(context, course) {
      const response = await http.put(`/courses/inactive?id=${course.id}&confirmedId=${course.confirmedBy}`);
      if (response.data.status == true) {
        course.status = "Inactive";
        context.commit("updateCourse", course);
      }
      return response;
    },
    async denyCourseStatus(context, course) {
      var data = {
        status: "Denied",
        id: course.id,
        confirmedBy: course.confirmedBy,
      };
      const response = await http.put(`/courses/confirm/${course.id}`, data);
      if (response.statusText == "OK") {
        if (response.data == true) {
          course.status = "Denied";
        }
        course.confirmerName = profileManager.state.account.fullname;
        context.commit("updateCourse", course);
      }
      return response;
    },

    async enableCourseStatus(context, course) {
      let data = {
        status: "Active",
        id: course.id,
        confirmedBy: course.confirmedBy,
      };

      const response = await http.put(`/courses/confirm/${course.id}`, data);
      if (response.statusText == "OK") {
        if (response.data == true) {
          course.status = "Active";
        }
        course.confirmerName = profileManager.state.account.fullname;
        context.commit("updateCourse", course);
      }
      return response;
    },
    async getCourseByTutorIdAction(context, tutorId) {
      let url = `/courses/check-course-by-tutor/${tutorId}`;
      try {
        const res = await http.get(url);
        return res;
      } catch (error) {
        console.log(error);
      }
    },
    async getCountCourseByClassHasSubjectAction(context, id) {
      let url = `courses/check-course-by-class-has-subject?id=${id}`;
      try {
        const res = await http.get(url);
        if (res.status === 200) {
          return res;
        } else {
          return 0;
        }
      } catch (error) {
        console.log(error);
      }
    },
    async getCourseDetailByCourseId(context, id) {
      const response = await http.get(`/course-detail/get-by-course/${id}`);
      if (response.status == 200) {
        let courseDetail = response.data;
        context.commit("setCourseDetail", courseDetail);
      }
      return response;
    },
    async getCourseById(context, id) {
      const response = await http.get(`/courses/get/${id}`);
      if (response.status == 200) {
        let courseDetail = response.data;
        context.commit("setCourse", courseDetail);
      }
      return response;
    },
  },
};
export default courseManament;
