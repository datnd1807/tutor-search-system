import http from "../../http-common";
import profileManager from "./ProfileManager";
const reportManagement = {
  state: {
    reportTutees: [],
    pagingReportTutee: {},
    reportTypeTutee: [],
    pagingReportTypeTutee: {},
    reportTutors: [],
    pagingReportTutor: {},
    reportTypeTutor: [],
    pagingReportTypeTutor: {},
    countReportTutorPending: 0,
    countReportTuteePending: 0,
  },
  getters: {
    getReportsTutee: (state) => {
      return state.reportTutees;
    },
    getPagingReportTutee: (state) => {
      return state.pagingReportTutee;
    },
    getReportTypeTutee: (state) => {
      return state.reportTypeTutee;
    },
    getPagingReportTypeTutee: (state) => {
      return state.pagingReportTypeTutee;
    },
    getReportsTutor: (state) => {
      return state.reportTutors;
    },
    getPagingReportTutor: (state) => {
      return state.pagingReportTutor;
    },
    getReportTypeTutor: (state) => {
      return state.reportTypeTutor;
    },
    getPagingReportTypeTutor: (state) => {
      return state.pagingReportTypeTutor;
    },
    getNumberReportTutor: (state) => {
      return state.countReportTutorPending;
    },
    getNumberReportTutee: (state) => {
      return state.countReportTuteePending;
    },
  },
  mutations: {
    setReportTutee(state, report) {
      state.reportTutees = report;
    },
    setPagingReportTutee(state, paging) {
      state.pagingReportTutee = paging;
    },
    setReportTypeTutee(state, reportType) {
      state.reportTypeTutee = reportType;
    },
    setPagingReportTypeTutee(state, paging) {
      state.pagingReportTypeTutee = paging;
    },
    setReportTutor(state, reportType) {
      state.reportTutors = reportType;
    },
    setPagingReportTutor(state, paging) {
      state.pagingReportTutor = paging;
    },
    setReportTypeTutor(state, reportType) {
      state.reportTypeTutor = reportType;
    },
    setPagingReportTypeTutor(state, paging) {
      state.pagingReportTypeTutor = paging;
    },
    setNumberReportTutorPending(state, number) {
      state.countReportTutorPending = number;
    },
    setNumberReportTuteePending(state, number) {
      state.countReportTuteePending = number;
    },
    updateReportTutee(state, data) {
      const index = state.reportTutees.findIndex((r) => r.id == data.id);
      if (index != -1) {
        state.reportTutees.splice(index, 1, data);
      }
    },
    updateReportTutor(state, data) {
      const index = state.reportTutors.findIndex((r) => r.id == data.id);
      if (index != -1) {
        state.reportTutors.splice(index, 1, data);
      }
    },
  },
  actions: {
    async getListReportTutees(context, filter) {
      const response = await http.get(`/tutee-report/filter/`, {
        params: {
          fromDate: filter.fromDate,
          toDate: filter.toDate,
          status: filter.status,
          reportType: filter.reportType,
          pageNumber: filter.pageNumber,
          pageSize: filter.pageSize,
          tuteeEmail: filter.email,
        },
      });
      if (response.status == 200) {
        let listReportTutee = response.data.data;
        let paging = response.data;
        context.commit("setReportTutee", listReportTutee);
        context.commit("setPagingReportTutee", paging);
      }
      return response;
    },
    async acceptReportTutee(context, data) {
      const response = await http.put(`/tutee-report/accept/${data.id}`, {
        id: data.id,
        confirmedBy: data.confirmedBy,
      });
      if (response.status == 200) {
        data.status = "Accepted";
        context.dispatch("getNumberReportTuteePending");
        data.confirmName = profileManager.state.account.fullname;
        context.commit("updateReportTutee", data);
      }
      return response;
    },
    async deniedReportTutee(context, data) {
      const response = await http.put(`/tutee-report/deny/${data.id}`, {
        id: data.id,
        confirmedBy: data.confirmedBy,
      });
      if (response.status == 200) {
        data.status = "Denied";
        context.dispatch("getNumberReportTuteePending");
        data.confirmName = profileManager.state.account.fullname;
        context.commit("updateReportTutee", data);
      }
      return response;
    },
    async getListReportTypeTutee(context, filter) {
      const response = await http.get(`/report-type/filter`, {
        params: {
          name: filter.name,
          status: filter.status,
          roleId: 4,
          pageNumber: filter.pageNumber,
          pageSize: filter.pageSize,
        },
      });
      if (response.status == 200) {
        let listReportType = response.data.data;
        let pagingReportType = response.data;
        context.commit("setReportTypeTutee", listReportType);
        context.commit("setPagingReportTypeTutee", pagingReportType);
      }
      return response;
    },
    async deactiveReportTypeTutee(context, data) {
      const response = await http.put(`/report-type/inactive/${data.id}`, {
        id: data.id,
        updatedBy: data.updatedBy,
      });
      if (response.status == 200) {
        data.status = "Inactive";
      }
      return response;
    },
    async activeReportTypeTutee(context, data) {
      const response = await http.put(`/report-type/active/${data.id}`, {
        id: data.id,
        updatedBy: data.updatedBy,
      });
      if (response.status == 200) {
        data.status = "Active";
      }
      return response;
    },
    async createReportTypeTutee(context, data) {
      const response = await http.post(`/report-type`, {
        name: data.name,
        description: data.description,
        createdBy: data.createdBy,
        roleId: 4,
        status: "Active",
      });
      if (response.status == 204) {
        var filter = {
          pageSize: 5,
        };
        await context.dispatch("getListReportTypeTutee", filter);
      }
      return response;
    },
    async getListReportTutors(context, filter) {
      const response = await http.get(`/tutor-report/filter/`, {
        params: {
          fromDate: filter.fromDate,
          toDate: filter.toDate,
          status: filter.status,
          reportType: filter.reportType,
          pageNumber: filter.pageNumber,
          pageSize: filter.pageSize,
          tutorEmail: filter.email,
        },
      });
      if (response.status == 200) {
        let listReportTutor = response.data.data;
        let paging = response.data;
        context.commit("setReportTutor", listReportTutor);
        context.commit("setPagingReportTutor", paging);
      }
      return response;
    },
    async deniedReportTutor(context, data) {
      const response = await http.put(`/tutor-report/deny/${data.id}`, {
        id: data.id,
        confirmedBy: data.confirmedBy,
      });
      if (response.status == 200) {
        data.status = "Denied";
        context.dispatch("getNumberReportTutorPending");
        data.confirmName = profileManager.state.account.fullname;
        context.commit("updateReportTutor", data);
      }
      return response;
    },
    async acceptReportTutor(context, data) {
      const response = await http.put(`/tutor-report/accept/${data.id}`, {
        id: data.id,
        confirmedBy: data.confirmedBy,
      });
      if (response.status == 200) {
        data.status = "Accepted";
        context.dispatch("getNumberReportTutorPending");
        data.confirmName = profileManager.state.account.fullname;
        context.commit("updateReportTutor", data);
      }
      return response;
    },
    async getListReportTypeTutor(context, filter) {
      const response = await http.get(`/report-type/filter`, {
        params: {
          name: filter.name,
          status: filter.status,
          roleId: 3,
          pageNumber: filter.pageNumber,
          pageSize: filter.pageSize,
        },
      });
      if (response.status == 200) {
        let listReportType = response.data.data;
        let pagingReportType = response.data;
        context.commit("setReportTypeTutor", listReportType);
        context.commit("setPagingReportTypeTutor", pagingReportType);
      }
      return response;
    },
    async deactiveReportTypeTutor(context, data) {
      const response = await http.put(`/report-type/inactive/${data.id}`, {
        id: data.id,
        updatedBy: data.updatedBy,
      });
      if (response.status == 200) {
        data.status = "Inactive";
      }
      return response;
    },
    async activeReportTypeTutor(context, data) {
      const response = await http.put(`/report-type/active/${data.id}`, {
        id: data.id,
        updatedBy: data.updatedBy,
      });
      if (response.status == 200) {
        data.status = "Active";
      }
      return response;
    },
    async createReportTypeTutor(context, data) {
      const response = await http.post(`/report-type`, {
        name: data.name,
        description: data.description,
        createdBy: data.createdBy,
        roleId: 3,
        status: "Active",
      });
      if (response.status == 204) {
        var filter = {
          pageSize: 5,
        };
        await context.dispatch("getListReportTypeTutor", filter);
      }
      return response;
    },
    async getNumberReportTutorPending(context) {
      const response = await http.get("/tutor-report/count-pending");
      if (response.status == 200) {
        context.commit("setNumberReportTutorPending", response.data);
      }
    },
    async getNumberReportTuteePending(context) {
      const response = await http.get("/tutee-report/count-pending");
      if (response.status == 200) {
        context.commit("setNumberReportTuteePending", response.data);
      }
    },
  },
};
export default reportManagement;
